﻿using System;
using Microsoft.Owin.Hosting;
using Nancy;
using Nancy.Bootstrapper;
using Nancy.TinyIoc;
using Owin;

namespace Transcribit.Api.api
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.UseNancy();
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var url = "http://+:8080";

            using (WebApp.Start<Startup>(url))
            {
                Console.WriteLine("Running on text {0}", url);
                Console.WriteLine("Press enter to exit");
                Console.ReadLine();
            }
        }
    }

    public class NancyBootStrapper : DefaultNancyBootstrapper
    {
        protected override void RequestStartup(TinyIoCContainer container, IPipelines pipelines, NancyContext context)
        {
            var origin = "*";
            pipelines.AfterRequest.AddItemToEndOfPipeline(ctx => ctx.Response
                .WithHeader("Access-Control-Allow-Origin", origin)
                .WithHeader("Access-Control-Allow-Methods", "POST,GET,PUT,OPTIONS,TRACE")
                .WithHeader("Access-Control-Allow-Credentials", "true")
                .WithHeader("Access-Control-Allow-Headers", "Accept, Origin, Content-type, Last-Event-ID, Content-Language, Accept-Language, X-Requested-With, Authorization"));
        }
    }
}
