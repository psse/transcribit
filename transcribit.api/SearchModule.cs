﻿using System;
using System.Configuration;
using Nancy;
using RestSharp;
using RestSharp.Deserializers;
using RestSharp.Serializers;

namespace Transcribit.Api
{
    public class SearchModule : NancyModule
    {
        private readonly ISearchService _searchService;
        public SearchModule(ISearchService searchService)
        {
            _searchService = searchService;
            Get["/search/{searchText}"] = s => _searchService.SearchByText(s.searchText);
        }
    }

    public class ElasticSearchService : ISearchService
    {

        public void IndexText(string fileName, string transcript)
        {
            var node = $"{ConfigurationManager.AppSettings["ElasticSearch:NodeUri"]}/transcript/files";
            var client = new RestClient(node);
            var request = new RestRequest(Method.POST);
            var body = new
            {
                fileName = fileName,
                transcript = transcript
            };
            request.AddJsonBody(body);
            client.Execute(request);
        }

        public dynamic SearchByText(string searchText)
        {

            var node = $"{ConfigurationManager.AppSettings["ElasticSearch:NodeUri"]}/_search";
       

            var client = new RestClient(node);
            var request = new RestRequest(Method.POST);
            var body = new
            {
                query = new
                {
                    term = new
                    {
                        /* value is field name in document*/
                        transcript = searchText
                    }
                }
            };
            request.AddJsonBody(body);

            var result = client.Execute(request);
            try
            {
                var hits = new JsonDeserializer().Deserialize<dynamic>(result)["hits"]["hits"];
                var documents = new JsonArray();

                foreach (var hit in hits)
                {
                    var index = hit["_index"];
                    var type = hit["_type"];
                    var transcript = hit["_source"]["transcript"];
                    var fileName = hit["_source"]["fileName"];
                    documents.Add(new {index,type,transcript, fileName });
                }
                return new JsonSerializer().Serialize(documents);
            }
            catch (Exception)
            {
                return "Någonting gick fel..";
            }

        }
    }

    public interface ISearchService
    {
        void IndexText(string fileName, string transcript);
        dynamic SearchByText(string searchText);
    }
}
