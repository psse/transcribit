﻿using System.IO;

namespace Transcribit.Api
{
    public class FileService
    {
        public void Save(Nancy.HttpFile file, string filePath)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);

            using (var newfile = File.Create(filePath))
                file.Value.CopyTo(newfile);
        }
        public void Save(string filePath, string text)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);
            File.AppendAllText(filePath,text);
        }

        public string ReadAll(string filePath) => File.ReadAllText(filePath);
    }
}