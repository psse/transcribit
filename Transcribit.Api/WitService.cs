﻿using System;
using RestSharp;

namespace Transcribit.Api
{
    public class WitService 
    {
        public string Get(string fileName, string filePath)
        {
            var client = new RestClient("https://api.wit.ai");

            var request = new RestRequest(new Uri("/speech?v=20141022", UriKind.Relative), Method.POST);
            request.AddHeader("Authorization", "Bearer WTTHXOFC4IQQESWBURCUTN4I75EN2RKB");
            request.AddHeader("Content-Type", "audio/wav");

            request.AddFile(fileName, filePath);

            var response = client.Execute(request);
            return response.Content;
        }
    }
}