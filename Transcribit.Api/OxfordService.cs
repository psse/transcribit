﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using Microsoft.ProjectOxford.SpeechRecognition;

namespace Transcribit.Api
{
    public class OxfordService
    {
        public string Get(string name, string filePath)
        {
            string result = null;

            var client = SpeechRecognitionServiceFactory.CreateDataClient(SpeechRecognitionMode.LongDictation,"en-US", "f5d71b911d48499390ad9f098913e1aa", "f5d71b911d48499390ad9f098913e1aa");
            client.OnResponseReceived += (o, e) =>
            {
                if (e.PhraseResponse.RecognitionStatus == RecognitionStatus.RecognitionSuccess)
                    result = string.Concat( e.PhraseResponse.Results.Select(s => s.DisplayText));
            };

            var stream = File.Open(filePath, FileMode.Open);
            var length = (int)stream.Length;
            var bufferSize = 1024;
            var rest = length % bufferSize;

            var send = new Action<int>(size =>
            {
                var buffer = new byte[size];
                stream.Read(buffer, 0, size);
                client.SendAudio(buffer, size);
            });

            for (var index = 0; index < length/bufferSize; index++)
                send(bufferSize);
            
            if (rest > 0)
                send(rest);
            
            client.EndAudio();

            while (true)
            {
                if (result != null)
                    return result;
                Thread.Sleep(500);
            }
        }
    }
}