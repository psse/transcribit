using System.Linq;
using Nancy;

namespace Transcribit.Api
{
    public class RootModule : Nancy.NancyModule
    {
        public RootModule(INancyModuleCatalog catalog)
        {
            Get["/"] = _ => Response.AsJson(catalog.GetAllModules(Context)
                .SelectMany(module => module.Routes.ToList()
                    .Select(s => new
                    {
                        s.Description.Path,
                        s.Description.Method
                    }))
                .OrderBy(s => s.Path));
        }
    }
}