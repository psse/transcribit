﻿using System;
using System.Linq;

namespace Transcribit.Api
{
    public class UploadModule : Nancy.NancyModule
    {
        private readonly FileService _fileService;
        private readonly ISearchService _searchService;

        public UploadModule(FileService fileService, OxfordService oxfordService, WitService witService, ISearchService searchService)
        {
            _fileService = fileService;
            _searchService = searchService;

            Post["/uploadfile/"] = _ => Upload(oxfordService.Get);
            Post["/uploadfilewit/"] = _ => Upload(witService.Get);
        }

        string Upload(Func<string,string,string> func)
        {
            var file = Request.Files.First();
            var filePath = "c:\\temp\\" + file.Name;
            _fileService.Save(file, filePath);
            var transcript = func(file.Name, filePath);
            _fileService.Save(filePath + ".transcript", transcript);
            _searchService.IndexText(file.Name, transcript);
            return transcript;
        }
    }
}
