namespace Transcribit.Api
{
    public class TranscriptModule : Nancy.NancyModule
    {
        public TranscriptModule(FileService fileService)
        {
            Get["/transcript/{fileName}"] = _ =>
            {
                var filePath  = "c:\\temp\\" + _.fileName + ".transcript";
                return fileService.ReadAll(filePath);
            };
        }
    }
}