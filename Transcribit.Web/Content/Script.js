﻿var tw = tw || {};


tw.actions = {
    appendResult: function (data, filename) {
        var $wrapper = $('<div class="row">');
        $wrapper.append('<p><span class="label">Filename</span><span class="field"><a href="">' + filename + '</a></span></p>');
        $wrapper.append('<p><span class="label">Content</span><span class="field">" ' + data + '"</span></p>');
        $("#result-container").prepend($wrapper);

    },

    fileUpload: function () {
        var $me = $("#singleupload");
        $me.show();
        $me.uploadFile({
            url: "http://localhost:8080/uploadfile",
            multiple: true,
            dragDrop: false,
            maxFileCount: 1,
            fileName: "myfile",
            onSuccess: function (files, data, xhr, pd) {
                var filename = $("#ajax-upload-id-1459508005142").val();
                tw.actions.appendResult(data, filename);
                $("#spinner").remove();
            },
            onSubmit: function (files) {
                $me.append("<i class=\"fa fa-cog fa-spin\" id=\"spinner\" style=\"font-size:30px;color:#ccc\"></i>");
                //return false;
            }
        });
    }
}


$(function () {
    "use strict";
    tw.actions.fileUpload();
});


