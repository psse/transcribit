﻿using System.Web.Mvc;
using RestSharp;

namespace Transcribit.Web.Controllers
{
    public class SearchController : Controller
    {
        // GET: Home
        public JsonResult Index(string q)
        {
            var client = new RestClient(string.Format("http://localhost:8080/search/{0}",q));
            var request = new RestRequest(Method.GET);
            var result = client.Execute(request);
            var content = result.Content;
            return Json(content, JsonRequestBehavior.AllowGet);
        }

    }
}